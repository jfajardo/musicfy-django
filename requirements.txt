Django==2.1
djangorestframework==3.8.2
Pillow==5.2.0
djoser==1.2.1
django-filter==2.0.0
django-crum==0.7.3