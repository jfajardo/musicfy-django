# Musicfy Django

APIS developed in Django and Django Rest Framework

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Installing

You need pip

```
pip install - requirements.txt
```

## Running the tests

To run the project

```
python manage.py runserver
```

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Django Rest Framework](http://www.django-rest-framework.org/) - Django REST framework is a powerful and flexible toolkit for building Web APIs.

## Authors

* **Jonhatan Fajardo** - - [My Works](https://jfajardo.github.com)
