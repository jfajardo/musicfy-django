from rest_framework.exceptions import PermissionDenied


def is_superuser(func):
    def wrap(self, *args, **kwargs):
        if self.request.user.is_superuser:
            return func(self, *args, **kwargs)
        else:
            raise PermissionDenied

    return wrap
