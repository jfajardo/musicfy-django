from django.db import models
from django.contrib.auth.models import User
from crum import get_current_user
from .choices import *


class Artist(models.Model):
    full_name = models.CharField(max_length=50)
    avatar = models.ImageField(upload_to='artists')

    class Meta:
        ordering = ['full_name', ]

    def __str__(self):
        return self.full_name


class Album(models.Model):
    name = models.CharField(max_length=20)
    cover = models.ImageField(upload_to='albums')
    description = models.TextField()
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    musical_genre = models.CharField(choices=MUSICAL_GENRE, max_length=20)

    class Meta:
        ordering = ['name', 'artist']

    def __str__(self):
        return self.name


class Track(models.Model):
    name = models.CharField(max_length=40)
    song = models.FileField(upload_to='tracks')
    album = models.ForeignKey(Album, related_name='tracks', on_delete=models.CASCADE)

    class Meta:
        ordering = ['name', 'album']

    def __str__(self):
        return self.name

    def is_favorite(self):
        return FavoriteTrack.objects.filter(track=self, user=get_current_user()).exists()


class FavoriteTrack(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'track']
