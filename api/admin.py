from django.contrib import admin
from .models import *

admin.site.register(Artist)
admin.site.register(Track)


@admin.register(Album)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'artist')


@admin.register(FavoriteTrack)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('track', 'user')