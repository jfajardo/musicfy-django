from django.urls import path, include
from .views import *

urlpatterns = [
    path('artists', ListArtistsView.as_view()),
    path('artist/<int:pk>', ArtistDetail.as_view()),
    path('albums', ListAlbumsView.as_view()),
    path('album/<int:pk>', AlbumTracksView.as_view()),
    path('favorite-track', FavoriteTrackView.as_view()),
    path('favorites', FavoriteTracksView.as_view()),
    path('auth', include('djoser.urls')),
    path('auth', include('djoser.urls.authtoken')),
]
