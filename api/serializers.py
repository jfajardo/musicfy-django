from rest_framework import serializers
from .models import *


class ArtistNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('full_name', )


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'

    def validate(self, data):
        """
        Validar que el nombre tenga más de 2 letras
        """
        if len(data['full_name']) <= 2:
            raise serializers.ValidationError("El nombre debe de contener más de 2 letras")
        return data


class AlbumSerializer(serializers.ModelSerializer):
    artist = ArtistNameSerializer()
    musical_genre = serializers.CharField(source='get_musical_genre_display')

    class Meta:
        model = Album
        fields = '__all__'


class TrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Track
        fields = ('id', 'name', 'song', 'is_favorite')


class AlbumTracksSerializer(serializers.ModelSerializer):
    tracks = TrackSerializer(many=True)

    class Meta:
        model = Album
        fields = ('tracks', )


class FavoriteTrackSerializer(serializers.ModelSerializer):
    track = TrackSerializer()

    class Meta:
        model = FavoriteTrack
        fields = ('track', )
