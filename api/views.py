from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend

from .pagination import MyPagination
from .serializers import *
from .decorators import *


class ListArtistsView(ListAPIView):
    serializer_class = ArtistSerializer
    filter_backends = (OrderingFilter, DjangoFilterBackend, SearchFilter)
    filter_fields = ('full_name', 'id')
    ordering_fields = ('full_name', 'id')
    search_fields = ('full_name', 'id')
    #pagination_class = MyPagination

    def get_queryset(self):
        return Artist.objects.all()
        #return Artist.objects.filter(full_name__icontains=self.request.GET['full_name'])

    @is_superuser
    def post(self, request):
        serializer = ArtistSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ArtistDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    """
    Obtener, actualizar o borrar una instancia de Artist
    """
    def get_object(self, pk):
        try:
            return Artist.objects.get(pk=pk)
        except Artist.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        artist = self.get_object(pk)
        serializer = ArtistSerializer(artist, context={'request': request})
        return Response(serializer.data)

    @is_superuser
    def put(self, request, pk):
        snippet = self.get_object(pk)
        serializer = ArtistSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @is_superuser
    def delete(self, request, pk):
        artist = self.get_object(pk)
        artist.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListAlbumsView(ListAPIView):
    serializer_class = AlbumSerializer
    filter_backends = (OrderingFilter, DjangoFilterBackend, SearchFilter)
    filter_fields = ('name', 'id', 'artist')
    ordering_fields = ('name', 'id')
    search_fields = ('name', 'id')

    def get_queryset(self):
        return Album.objects.all()


class AlbumTracksView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)

    def get_object(self, pk):
        try:
            return Album.objects.get(pk=pk)
        except Album.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        album = self.get_object(pk)
        serializer = AlbumTracksSerializer(album, context={'request': request})
        return Response(serializer.data)


class FavoriteTrackView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)

    def post(self, request):
        try:
            track_id = request.data['track']
            FavoriteTrack.objects.create(user=request.user, track_id=track_id)
            return Response({'is_favorite': True}, status=status.HTTP_200_OK)
        except IntegrityError:
            FavoriteTrack.objects.get(user=request.user, track_id=track_id).delete()
            return Response({'is_favorite': False}, status=status.HTTP_200_OK)
        except Exception:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class FavoriteTracksView(ListAPIView):
    serializer_class = FavoriteTrackSerializer
    authentication_classes = (SessionAuthentication, TokenAuthentication)

    def get_queryset(self):
        return FavoriteTrack.objects.filter(user=self.request.user)
